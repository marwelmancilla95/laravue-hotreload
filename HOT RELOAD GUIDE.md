HOT RELOAD
1. npm install --save-dev cross-env

2. Change in html
- Original
```<script src="{{asset('js/app.js')}}"></script>```
    
- For Hot Reload 
```<script src="{{ mix('js/app.js') }}"></script>```

3. Add to package.json scripts.
```"hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --hot --config=node_modules/laravel-mix/setup/webpack.config.js --port 8000",```

4. Add to webpack.mix.js
```
mix.options({
    hmrOptions: {
        host: "localhost",
        port: 8000,
    },
});
```
